#include "novoproduto.hpp"
#include "venda.hpp"
#include <fstream>
#include <iostream>
#include <string>

using namespace std;

NovoProduto::NovoProduto(){
	string nome_in;
	float preco_in;
	int quantidade_in;
	string tipo_in;
	int escolha = 0;
	cout << "Insira o tipo e o nome do produto, preço e quantidade para ser adicionada ao estoque." << endl;
	cout << "Digite o tipo do produto: ";
	getline(cin >> ws, tipo_in);
	cout << "Nome: ";
	getline(cin >> ws, nome_in);
	cout << "Preço: R$ ";
	cin >> preco_in;
	cout << "Quantidade: ";
	cin >> quantidade_in;
	cout << "\n";

	Produtos produto(tipo_in, nome_in, preco_in, quantidade_in);

	system("clear");
	cout << "Produto adicionado ao estoque.\n";
	cout << "Deseja adicionar outro produto (S/N)?:" << endl;
	cin.clear();
	cin >> escolha;
	if (toupper(escolha) == 'S'){
		NovoProduto();
	}
	else if(toupper(escolha) == 'N'){
		
	}
}
NovoProduto::~NovoProduto(){

}