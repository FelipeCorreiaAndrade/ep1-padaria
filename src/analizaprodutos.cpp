#include "analizaprodutos.hpp"
#include "produtos.hpp"
#include <vector>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>

using namespace std;


AnalizaProdutos::AnalizaProdutos(){

	ifstream contador;
	int contador_codigo;
	contador.open("./db/etc/contadorcodigo.txt");
	if (contador.is_open()){
		while(contador >> contador_codigo){
			contador_codigo = contador_codigo -1;
		}
		contador.close();
	}

	ifstream pasta;
	int codigo;
	string tipo;
	int quantidade;
	string nome;
	float preco;

	for(int i = 1; i <= contador_codigo; i++){

		pasta.open("./db/produtos/" + to_string(i) + ".txt");
		if(pasta.is_open()){
			while(pasta >> codigo >> tipo >> nome >> preco >> quantidade){

			}
			listaprodutos.push_back(Produtos(codigo, tipo, nome, preco, quantidade));
			pasta.close();
		}	
	}
}

AnalizaProdutos::~AnalizaProdutos(){

}

void AnalizaProdutos::mostraProdutos(){
	cout << "\t### Produtos ###\n" << endl;
	cout << "Tipo\t(Codigo)Nome\t\tPreço\n" << endl;
	for (unsigned int i = 0; i < listaprodutos.size(); i++){
		cout << listaprodutos[i].get_tipo() << "\t\t" << "(" << listaprodutos[i].get_codigo() << ")" 
		<<listaprodutos[i].get_nome() << "\t\t" << "R$ " << fixed << setprecision(2) << listaprodutos[i].get_preco() << endl;
	}
}

void AnalizaProdutos::atualizarQuantidade(){
	cout << "\t### Produtos ###\n" << endl;
	cout << "Tipo\t(Codigo)Nome\t\tQuantidade\n" << endl;
	for (unsigned int i = 0; i < listaprodutos.size(); i++){
		cout << listaprodutos[i].get_tipo() << "\t\t" << "(" << listaprodutos[i].get_codigo() << ")" 
		<<listaprodutos[i].get_nome() << "\t\t" << listaprodutos[i].get_quantidade() << endl;
	}
	cout << "\nDigite o codigo do produto no qual deseja atualizar: ";

	int temp_codigo;
	cin >> temp_codigo;
	
	for (unsigned int i = 0; i < listaprodutos.size(); i++){
		if (listaprodutos[i].get_codigo() == temp_codigo){

			cout << "Quantidade atual do produto " + listaprodutos[i].get_nome() + " é de: " 
			+ to_string(listaprodutos[i].get_quantidade()) << endl;

			cout << "Digite quantas unidades desse produto você deseja adicionar ou retirar do estoque: ";
			
			int temp_quant;
			cin >> temp_quant;
			
			temp_quant = temp_quant + listaprodutos[i].get_quantidade();
			listaprodutos[i].set_quantidade(temp_quant);

			listaprodutos[i].guardar_produto(listaprodutos[i].get_codigo(), listaprodutos[i].get_tipo(), 
				listaprodutos[i].get_nome(), listaprodutos[i].get_preco(), listaprodutos[i].get_quantidade());
		}
	}
	cout << "Deseja atualizar estoque de outro produto? (S/N): ";
	
	cin.clear();
	char escolha;
	cin >> escolha;
	
	if(toupper(escolha) == 'S'){
		atualizarQuantidade();
	}
	else if(toupper(escolha) == 'N'){
	 
	}
}